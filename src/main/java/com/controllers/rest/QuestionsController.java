package com.controllers.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.constants.URIConstants;
import com.models.inst.Question;
import com.models.ref.RestResponse;
import com.mongodb.util.JSON;
import com.services.impl.QuestionServiceImpl;
import com.utils.temp.CommonUtils;

@RestController
public class QuestionsController {

	@Autowired
	QuestionServiceImpl questionService;
	
	@Autowired
	RestResponse response;
	
	@RequestMapping({URIConstants.GET_ALL_TOP_QUESTIONS_1, URIConstants.GET_ALL_TOP_QUESTIONS_2})
	public @ResponseBody List<Question> getRelatedQuestions(){
		return questionService.getRelated();
	}
	
	@RequestMapping({URIConstants.GET_QUESTION})
	public @ResponseBody Question getQuestion(@PathVariable String id){
		System.out.println(id);
		Question question = questionService.get(id);
		System.out.println(question);
		return question;
	}
	
	
	@RequestMapping(value = URIConstants.SAVE_QUESTIONS, method = RequestMethod.POST)
	public @ResponseBody RestResponse saveQuestion(@RequestBody Question question)
	{
		System.out.println(question.toString());
		questionService.save(question);		
		System.out.println(question.getId());
		System.out.println("Question saved"+ question.getQuestion());
		response = CommonUtils.getRestResponse(200, "OK", question);
		return response;
	}
}
