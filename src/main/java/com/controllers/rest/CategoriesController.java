package com.controllers.rest;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.constants.URIConstants;
import com.models.ref.Category;


@RestController
public class CategoriesController {

	private static final Logger logger = LoggerFactory.getLogger(CategoriesController.class);
    
	@RequestMapping({URIConstants.GET_ALL_CATEGORIES_1,URIConstants.GET_ALL_CATEGORIES_2})
	public @ResponseBody List<Category> getCategories(){
		logger.debug("Started getCategories");
		Category cat = new Category(Long.valueOf(1), "Computers", "Computers - This is some generic description", true);
		Category cat2 = new Category(Long.valueOf(2), "Literature", "Literature - This is some generic description", true);
		Category cat3 = new Category(Long.valueOf(3), "Comedy", "Comedy - This is some generic description", true);
		Category cat4 = new Category(Long.valueOf(4), "Psychology", "Psychology - This is some generic description", true);
		
		List<Category> categories = new ArrayList<Category>();
		categories.add(cat);
		categories.add(cat2);
		categories.add(cat3);
		categories.add(cat4);
		return categories;
	}
}
