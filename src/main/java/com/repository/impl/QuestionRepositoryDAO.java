package com.repository.impl;
import static org.springframework.data.mongodb.core.query.Criteria.*;
import static org.springframework.data.mongodb.core.query.Query.*;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.models.inst.Question;
import com.repository.QuestionRepository;

@Repository
public class QuestionRepositoryDAO implements QuestionRepository{

	@Autowired
	private MongoOperations operations;
	
	public void save(Question question)
	{
		operations.save(question);
	}

	public Question findOne(String id) {
		// TODO Auto-generated method stub
		Query query = Query.query(where("id").is(id));
		return operations.findOne(query, Question.class);
		
	}

	public List<Question> findAll() {
		// TODO Auto-generated method stub
		return operations.findAll(Question.class);
	}

	


}
