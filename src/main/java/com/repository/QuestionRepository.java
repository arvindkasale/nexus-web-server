package com.repository;

import java.util.List;

import org.springframework.data.repository.Repository;

import com.models.inst.Question;


public interface QuestionRepository extends Repository<Question, String> {
	
	public void save(Question question);
	
	public Question findOne(String id);
	
	public List<Question> findAll();
}
