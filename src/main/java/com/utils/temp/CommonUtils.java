package com.utils.temp;


import org.springframework.stereotype.Component;

import com.models.ref.RestResponse;

@Component
public class CommonUtils {
	
	
	public static RestResponse getRestResponse(int code, String status, Object object){
	RestResponse restResponse = new RestResponse();
	restResponse.setCode(code);
	restResponse.setStatus(status);
	restResponse.setData(object);
	return restResponse;	
	}
	
}
