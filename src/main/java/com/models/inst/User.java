package com.models.inst;

public class User {

	private String name, alias,avatarUrl;
	private int views;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAlias() {
		return alias;
	}
	public void setAlias(String alias) {
		this.alias = alias;
	}
	public String getAvatarUrl() {
		return avatarUrl;
	}
	public void setAvatarUrl(String avatarUrl) {
		this.avatarUrl = avatarUrl;
	}
	public int getViews() {
		return views;
	}
	public void setViews(int views) {
		this.views = views;
	}
	public User(String name, String alias, String avatarUrl, int views) {
		super();
		this.name = name;
		this.alias = alias;
		this.avatarUrl = avatarUrl;
		this.views = views;
	}
	
	public User(){};	
	
}
