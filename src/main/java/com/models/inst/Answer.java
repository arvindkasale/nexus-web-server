package com.models.inst;

public class Answer {

	private Long id;
	private String answer, imageUrl;
	private int votes, comments;
	private User user;
	private Question question;
	private String written_on;
	
	public Answer(){}
	
	public Answer(Long id, String answer, int votes, int comments, User user, Question question, String written_on, String imageUrl) {
		super();
		this.id = id;
		this.answer = answer;
		this.votes = votes;
		this.comments = comments;
		this.user = user;
		this.question = question;
		this.written_on = written_on;
		this.imageUrl = imageUrl;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	public int getVotes() {
		return votes;
	}
	public void setVotes(int votes) {
		this.votes = votes;
	}
	public int getComments() {
		return comments;
	}
	public void setComments(int comments) {
		this.comments = comments;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Question getQuestion() {
		return question;
	}
	public void setQuestion(Question question) {
		this.question = question;
	}
	public String getWritten_on() {
		return written_on;
	}
	public void setWritten_on(String written_on) {
		this.written_on = written_on;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	
	
}
