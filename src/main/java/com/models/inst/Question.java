package com.models.inst;

import java.util.Date;
import java.util.Set;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.models.ref.Category;

public class Question {

	@Id
	private String id;
	private String question, imageUrl;
	private String description;
	private User user;
	private Set<Answer> answers;
	private String written_on;
	private int followers, commentCount;
	private Set<Comment> comments;
	private Category category;
	
	@CreatedDate
	private Date createdAt;
	
	public Question(){
		
	}
	
	public Question(String id, String question, String image_url, User user, Set<Answer> answers, String written_on,
			int followers, int commentCount, Set<Comment> comments, Date createAt) {
		super();
		this.id = id;
		this.question = question;
		this.imageUrl = image_url;
		this.user = user;
		this.answers = answers;
		this.written_on = written_on;
		this.followers = followers;
		this.commentCount = commentCount;
		this.comments = comments;
		this.createdAt = createAt;
	}
	
	
	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public String getImage_url() {
		return imageUrl;
	}
	public void setImage_url(String image_url) {
		this.imageUrl = image_url;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Set<Answer> getAnswers() {
		return answers;
	}
	public void setAnswers(Set<Answer> answers) {
		this.answers = answers;
	}
	public String getWritten_on() {
		return written_on;
	}
	public void setWritten_on(String written_on) {
		this.written_on = written_on;
	}
	public int getFollowers() {
		return followers;
	}
	public void setFollowers(int followers) {
		this.followers = followers;
	}
	public int getCommentCount() {
		return commentCount;
	}
	public void setCommentCount(int commentCount) {
		this.commentCount = commentCount;
	}
	public Set<Comment> getComments() {
		return comments;
	}
	public void setComments(Set<Comment> comments) {
		this.comments = comments;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	@Override
	public String toString() {
		return "Question [id=" + id + ", question=" + question + ", imageUrl=" + imageUrl + ", description="
				+ description + ", user=" + user + ", answers=" + answers + ", written_on=" + written_on
				+ ", followers=" + followers + ", commentCount=" + commentCount + ", comments=" + comments
				+ ", category=" + category + ", createdAt=" + createdAt + "]";
	}

	
	
	
}
