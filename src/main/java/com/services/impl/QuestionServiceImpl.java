package com.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.models.inst.Question;
import com.repository.impl.QuestionRepositoryDAO;
import com.services.QuestionService;

@Service
public class QuestionServiceImpl implements QuestionService {
	
	@Autowired
	private QuestionRepositoryDAO questionDAO;

	public void save(Question question) {

		questionDAO.save(question);
	}

	public Question get(String id) {
		// TODO Auto-generated method stub
		return questionDAO.findOne(id);
	}

	public List<Question> getRelated() {
		// TODO Auto-generated method stub
		return questionDAO.findAll();
	}
	
}
