package com.services;

import java.util.List;

import com.models.inst.Question;

public interface QuestionService {

	void save(Question question);
	Question get(String id);
	List<Question> getRelated();
}