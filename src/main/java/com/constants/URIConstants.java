package com.constants;

public class URIConstants {

	public static final String GET_ALL_CATEGORIES_1 = "/categories/"; 
	public static final String GET_ALL_CATEGORIES_2 = "/categories/index";
	public static final String GET_ALL_TOP_QUESTIONS_1 = "/questions/";
	public static final String GET_ALL_TOP_QUESTIONS_2 = "/questions/index"; 
	public static final String SAVE_QUESTIONS = "/questions/save";
	public static final String GET_QUESTION = "/questions/{id}";
}
